/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.io.Serializable;

/**
 *
 * @author JULIO JOSE SAAVEDRA
 */
public class Servidor implements Serializable{
    String iporigen;
    String ipdestino;
    String usuario;
    String chat;

    public Servidor() {
    }

    public Servidor(String iporigen, String ipdestino, String usuario, String chat) {
        this.iporigen = iporigen;
        this.ipdestino = ipdestino;
        this.usuario = usuario;
        this.chat = chat;
    }

    public String getIporigen() {
        return iporigen;
    }

    public void setIporigen(String iporigen) {
        this.iporigen = iporigen;
    }

    public String getIpdestino() {
        return ipdestino;
    }

    public void setIpdestino(String ipdestino) {
        this.ipdestino = ipdestino;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }
    
    
    
    
}
